package cn.gson.oasys;

import java.text.SimpleDateFormat;
import  java.time.*;
import java.util.Date;

/**
 * @Auther: qiaojk
 * @Date: 2020/8/20 14:42
 * @Description:
 */
public class Demo {
    public static void  main(String[] args){
        LocalDate date = LocalDate.now();
        System.out.println(date.toString());

        Date nowDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       // LocalDate s = LocalDate.parse(nowDate.toString());
        System.out.println(sdf.format(nowDate));
    }
}
