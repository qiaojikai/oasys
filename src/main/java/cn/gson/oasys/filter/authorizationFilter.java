package cn.gson.oasys.filter;

import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Auther: qiaojk
 * @Date: 2020/8/18 14:21
 * @Description:
 */
@Order(1)
@WebFilter(filterName = "authorizationFilter", urlPatterns = "/*")
public class authorizationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("----secondDomainFilter-----");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String serverName = request.getServerName();
        HeaderHttpServletRequestWrapper headerRequest = new HeaderHttpServletRequestWrapper((HttpServletRequest) request);
        String regex = "\\d+\\.\\d+\\.\\d+\\.\\d+";
        if(!serverName.matches(regex)){
            int indexDomain = serverName.indexOf(".");
            if(indexDomain > 0) {
                String secondDomain = serverName.substring(0, indexDomain);
                headerRequest.addHeader("Authorization",secondDomain);
            }
        }
        chain.doFilter(headerRequest,response);
    }

    @Override
    public void destroy() {

    }
}
