package cn.gson.oasys.filter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * XSS过滤处理
 * 
 */
public class HeaderHttpServletRequestWrapper extends HttpServletRequestWrapper
{
    /**
     * @param request
     */
    public HeaderHttpServletRequestWrapper(HttpServletRequest request)
    {
        super(request);
    }
    
    private Map<String,String> headerMap = new ConcurrentHashMap<String,String>();
    
    public void addHeader(String key,String value){
    	headerMap.put(key, value);
    }
    
    @Override
    public String getHeader(String name) {
        String headerValue = super.getHeader(name);
        if(headerMap.containsKey(name)){
        	headerValue = headerMap.get(name);
        }
        return headerValue;
    }

}