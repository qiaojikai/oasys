package cn.gson.oasys.services;

import cn.gson.oasys.model.entity.user.User;
import cn.gson.oasys.utils.HttpRequestUtil;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 系统相关操作
 */
@Service
@Transactional
public class SystemService {

    private static final Logger logger = LoggerFactory.getLogger(SystemService.class);

    @Value("${APP_ID}")
    private String APP_ID;

    @Value("${APP_KEY}")
    private String APP_KEY;

    @Value("${BASE_URL}")
    private String BASE_URL;

    private static JSONObject TOKEN_JSON=new JSONObject();

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static volatile ConcurrentHashMap<String ,String> secondDomainMap = new ConcurrentHashMap<String ,String>();

    /**
     * 获取token
     * @return
     */
    public   String getToken(){
        String token= TOKEN_JSON.getString("token");
        if(token==null||(new Date().getTime()>TOKEN_JSON.getLong("timeout"))){//token为空或过期
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("appid", APP_ID);
            jsonObject.put("appkey",APP_KEY);
            logger.info("获取token入参："+jsonObject.toString());
            String jsonStr = HttpRequestUtil.sendPost(BASE_URL+"/csaas/api/access_token", jsonObject.toString());
            logger.info("获取token返参："+jsonStr);
            JSONObject jsonAccessToken = JSONObject.parseObject(jsonStr);
            String access_token = jsonAccessToken.getString("access_token");
            TOKEN_JSON.put("token",access_token);
            TOKEN_JSON.put("timeout",new Date().getTime()+7200000);//设置过期时间
        }
        token = TOKEN_JSON.getString("token");
        logger.info("获取token结果："+token);
        return token;
    }

    public String getSysIdBySecondDomain(String secondDomain){
        String sysId = secondDomainMap.get(secondDomain);
        if(null != sysId && !"".equals(sysId)){
            return sysId;
        }
        String token = this.getToken();
        String jsonStr = HttpRequestUtil.sendGet(BASE_URL+"/csaas/api/getSysIdBySecondDomain?access_token="+token+"&secondDomain="+secondDomain);
        JSONObject retJson = JSONObject.parseObject(jsonStr);
        if(retJson.getBoolean("status")){
            secondDomainMap.put(secondDomain,retJson.getString("data"));
            return retJson.getString("data");
        }
        return "";
    }


    /**
     * 调用平台接口注册账号
     * @param user
     */
    public JSONObject registerUser(User user,String sysId) {
        JSONObject userObj = new JSONObject();
        userObj.put("sysid",sysId);
        userObj.put("loginName",user.getUserName());
        userObj.put("name", user.getRealName());
        userObj.put("password","123456");
        userObj.put("register_time", LocalDate.now().toString());
        userObj.put("email",user.getEamil());
        userObj.put("gender","男".equals(user.getSex()) ? "0" : "1");
        try {
            userObj.put("birth",sdf.format(user.getBirth()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        userObj.put("mobile_phone",user.getUserTel());
        userObj.put("company","");
        userObj.put("province","");
        userObj.put("city","");
        userObj.put("address",user.getAddress());
        userObj.put("major","");
        userObj.put("education",user.getUserEdu());
        String token=this.getToken();
        logger.info("调用平台接口注册用户入参："+userObj.toString());
        String jsonStr = HttpRequestUtil.sendPost(BASE_URL +"/csaas/api/addPtUser?access_token=" + token, userObj.toString());
        logger.info("调用平台接口注册用户返参："+jsonStr);
        JSONObject jsonObj = JSONObject.parseObject(jsonStr);
        return jsonObj;
    }

    /**
     * 调用平台接口记录日志
     * @param username
     * @param sysId
     */
    public JSONObject writeLog(String username,String sysId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("loginName",username);
        jsonObject.put("sysid",sysId);
        jsonObject.put("operation","查看");
        jsonObject.put("object","进入系统");
        jsonObject.put("data",username+"登录系统");
        String token=this.getToken();
        logger.info("调用平台接口记录日志入参："+jsonObject.toString());
        String jsonStr = HttpRequestUtil.sendPost(BASE_URL +"/csaas/api/writeLog?access_token=" + token, jsonObject.toString());
        logger.info("调用平台接口记录日志返参："+jsonStr);
        JSONObject jsonObj = JSONObject.parseObject(jsonStr);
        return jsonObj;
    }

    /**
     * 检查系统有效性
     * @return
     */
    public JSONObject checkSysStatus(String sysId){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("sysid",sysId);
        String token = this.getToken();
        logger.info("检查系统有效性入参："+jsonObject.toString());
        String jsonStr = HttpRequestUtil.sendPost(BASE_URL + "/csaas/api/getSystemInfo?access_token=" + token, jsonObject.toString());
        logger.info("检查系统有效性出参："+jsonStr);
        JSONObject jsonObj = JSONObject.parseObject(jsonStr);
        return jsonObj;
    }
}
