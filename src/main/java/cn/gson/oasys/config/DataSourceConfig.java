package cn.gson.oasys.config;

/**
 * @Auther: qiaojk
 * @Date: 2020/8/18 10:11
 * @Description:
 */


import java.util.Properties;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.cserver.saas.system.datasource.ApplicationDataSource;

/**
 * 数据源的设置
 */
@Configuration
@Component
@MapperScan({"cn.gson.oasys.mappers","cn.gson.oasys.model.dao"})
@EnableTransactionManagement
public class DataSourceConfig {

    @Autowired
    private ApplicationDataSource applicationDataSource;

    /**
     * 数据源工厂配置
     * @return 数据源工厂
     * @throws Exception
     */
    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();

        //配置多租户的数据源
        sqlSessionFactoryBean.setDataSource(applicationDataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mappers/*.xml"));
        Properties properties = new Properties();
        properties.setProperty("helperDialect", "oracle");
        properties.setProperty("offsetAsPageNum", "true");
        properties.setProperty("rowBoundsWithCount", "true");
        properties.setProperty("reasonable", "true");
        properties.setProperty("supportMethodsArguments", "true");
        properties.setProperty("params", "pageNum=pageNumKey;pageSize=pageSizeKey;");
        return sqlSessionFactoryBean.getObject();

    }

}

