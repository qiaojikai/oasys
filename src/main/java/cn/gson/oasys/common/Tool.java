package cn.gson.oasys.common;

import javax.servlet.http.HttpServletRequest;

import cn.gson.oasys.filter.HeaderHttpServletRequestWrapper;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class Tool {

	public static  <T>T getBean(Class<T> clazz, HttpServletRequest request) {
		WebApplicationContext wc=   WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
		return wc.getBean(clazz);
	}


	public static String getSecondDomain(HttpServletRequest request){
		String serverName = request.getServerName();
		String regex = "\\d+\\.\\d+\\.\\d+\\.\\d+";
		if(!serverName.matches(regex)){
			int indexDomain = serverName.indexOf(".");
			if(indexDomain > 0) {
				return serverName.substring(0, indexDomain);
			}
		}
		return "";
	}
}
